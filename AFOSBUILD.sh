rm -rf /opt/ANDRAX/pipal

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

cp -Rf $(pwd) /opt/ANDRAX/pipal

find /opt/ANDRAX/pipal/checkers_available/ -name \*.rb -exec ln -s "{}" /opt/ANDRAX/pipal/checkers_enabled/ ';'

gem install levenshtein-ffi

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
